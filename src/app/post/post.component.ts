import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.css']
})
export class PostComponent {
    @Input() public postTitle: string;
    @Input() public postContent: string;
    @Input() public postLoveIt = 0;
    @Input() public postCreated_at: Date;

    constructor() { }

    public increment() {
        this.postLoveIt++;
    }
    public decrement() {
        this.postLoveIt--;
    }

    public getColor() {
        if (this.postLoveIt > 0) {
            return 'green';
        } else if (this.postLoveIt < 0) {
            return 'red  ';
        }
    }
}
