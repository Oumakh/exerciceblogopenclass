import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    public postList = [
        {
            title: 'Mon premier post',
            content: 'Ce programme vise à former des personnes aptes à développer des applications informatiques,' +
                'à effectuer du support technique et à procéder à l\’administration et à la gestion de réseaux' +
                'Pour ce faire, le programme est d\’abord et avant tout orienté vers la programmation d\’applications.' +
                'Il comporte un volet sur la réseautique et un autre sur l’installation et la configuration matérielles et' +
                'logicielles de postes de travail.',
            loveIts: 0,
            created_at: Date()

        },
        {
            title: 'Mon deuxième post',
            content: 'Au terme de leur formation, les programmeurs maîtriseront :' +

                'Les techniques et les méthodes de programmation;' +
                'Les techniques et les méthodes de conception de systèmes généralement utilisées;' +
                'Le fonctionnement de base de l’ordinateur et ses liens avec les périphériques locaux.',
            loveIts: 0,
            created_at: Date()

        },
        {
            title: 'Encore un post',
            content: 'Au cours de sa formation, l’étudiant participe à un stage en entreprise qui lui' +
                'permet non seulement d’accumuler de l’expérience, mais aussi d’établir des contacts avec un employeur éventuel.' +
                'Au terme de sa formation, le technicien sera en mesure d\'analyser les besoins en matière de traitement de' +
                'l\'information, de participer à toutes les étapes de la conception de systèmes, de réaliser, de modifier,' +
                'd\'adapter, de tester et de valider des logiciels, de mettre en oeuvre efficacement des systèmes de gestion' +
                'de bases de données et d\'accomplir des tâches spécifiques à la gestion de réseaux en utilisant la technologie CISCO.',
            loveIts: 0,
            created_at: Date()

        }

    ];

    public constructor() {}
}
